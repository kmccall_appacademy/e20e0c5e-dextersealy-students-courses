class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    return if enrolled_in? new_course
    raise "conflict detected"  if has_conflict? new_course
    add_course new_course
  end

  def enrolled_in?(course)
    @courses.include? course
  end

  def has_conflict?(new_course)
    @courses.any? { |course| course.conflicts_with?(new_course) }
  end

  def course_load
    department_credits = Hash.new(0)

    @courses.each do |course|
      department_credits[course.department] += course.credits
    end

    department_credits
  end

  private

  def add_course(new_course)
    @courses << new_course
    new_course.students << self
  end

end
